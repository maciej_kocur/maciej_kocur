package pl.mk.gs.bl.spi;

import pl.mk.gs.bl.api.UserId;
import pl.mk.gs.bl.spi.data.UserState;

public interface UserDao {

	void save(UserState userState);

	UserState load(UserId userId);
}
