package pl.mk.gs.bl.spi.data;

import pl.mk.gs.bl.api.UserId;

public class UserState {
	private UserId id;
	private String name;

	public UserId getId() {
		return id;
	}

	public void setId(UserId id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
