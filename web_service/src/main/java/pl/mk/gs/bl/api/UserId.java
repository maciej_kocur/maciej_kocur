package pl.mk.gs.bl.api;

import java.util.UUID;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class UserId {

	public abstract String asString();

	public static UserId from(String string) {
		return new AutoValue_UserId(string);
	}

	public static UserId generate() {
		return from(UUID.randomUUID().toString());
	}
}
