package pl.mk.gs.bl.api;

import java.util.UUID;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class GroupId {

	public abstract String asString();

	public static GroupId from(String string) {
		return new AutoValue_GroupId(string);
	}

	public static GroupId generate() {
		return from(UUID.randomUUID().toString());
	}
}
