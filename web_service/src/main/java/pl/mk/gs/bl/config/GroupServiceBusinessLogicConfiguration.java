package pl.mk.gs.bl.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pl.mk.gs.bl.api.GroupService;
import pl.mk.gs.bl.impl.GroupServiceImpl;
import pl.mk.gs.bl.spi.GroupDao;
import pl.mk.gs.bl.spi.UserDao;
import web_service.SettableCurrentUserService;

@Configuration
public class GroupServiceBusinessLogicConfiguration {

	@Bean
	public GroupService groupService(
			SettableCurrentUserService currentUserService,
			UserDao userDao,
			GroupDao groupDao) {
		return new GroupServiceImpl(currentUserService, userDao, groupDao);
	}
}
