package pl.mk.gs.bl.api;

public interface GroupService {
	UserId register(String name);
	GroupId createGroup(String name);
	void joinGroup(GroupId groupId);
	String showGroup(GroupId groupId);
}
