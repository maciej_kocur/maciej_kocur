package pl.mk.gs.bl.spi;

import pl.mk.gs.bl.api.UserId;

public interface CurrentUserService {

	UserId getCurrentUserId();
}
