package pl.mk.gs.bl.impl;

import java.util.LinkedList;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;

import pl.mk.gs.bl.api.GroupId;
import pl.mk.gs.bl.api.GroupService;
import pl.mk.gs.bl.api.UserId;
import pl.mk.gs.bl.spi.GroupDao;
import pl.mk.gs.bl.spi.UserDao;
import pl.mk.gs.bl.spi.data.GroupState;
import pl.mk.gs.bl.spi.data.UserState;
import web_service.SettableCurrentUserService;

public class GroupServiceImpl implements GroupService {

	private static final Logger LOG = Logger.getLogger(GroupServiceImpl.class.getSimpleName());

	private final SettableCurrentUserService currentUserService;
	private final UserDao userDao;
	private final GroupDao groupDao;

	@Autowired
	public GroupServiceImpl(
			SettableCurrentUserService currentUserService,
			UserDao userDao,
			GroupDao groupDao) {
		this.currentUserService = currentUserService;
		this.userDao = userDao;
		this.groupDao = groupDao;
	}

	@Override
	public UserId register(String userName) {
		UserState userState = new UserState();
		UserId userId = UserId.generate();
		userState.setId(userId);
		userState.setName(userName);
		userDao.save(userState);
		currentUserService.setCurrentUserId(userId);
		return userId;
	}

	@Override
	public GroupId createGroup(String groupName) {
		GroupState groupState = new GroupState();
		GroupId groupId = GroupId.generate();
		UserId currrentUserId = currentUserService.getCurrentUserId();
		groupState.setId(groupId);
		groupState.setName(groupName);
		groupState.setGroupOwner(currrentUserId);
		groupState.setUsersList(new LinkedList<>());
		groupState.addUser(currrentUserId);
		groupDao.save(groupState);
		return groupId;
	}

	@Override
	public void joinGroup(GroupId groupId) {
		UserId currentUserId = currentUserService.getCurrentUserId();
		LOG.info(String.format("user %s chce dolaczyc do grupy %s", currentUserId, groupId));
		GroupState groupState = groupDao.load(groupId);
		groupState.addUser(currentUserId);
		groupDao.save(groupState);
	}

	@Override
	public String showGroup(GroupId groupId) {
		GroupState groupState = groupDao.load(groupId);
		StringBuilder groupInfo = new StringBuilder()
				.append(groupState.getName())
				.append(", Owner: ")
				.append(userDao.load(groupState.getGroupOwner()).getName())
				.append(" (");
		for (UserId userId : groupState.getUsersList()) {
			groupInfo
					.append(" ")
					.append(userDao.load(userId).getName())
					.append(";");
		}
		groupInfo.append(")");
		LOG.info(groupInfo.toString());
		return groupInfo.toString();
	}
}
