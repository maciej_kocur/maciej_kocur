package pl.mk.gs.bl.spi;

import pl.mk.gs.bl.api.GroupId;
import pl.mk.gs.bl.spi.data.GroupState;

public interface GroupDao {

	void save(GroupState groupState);

	GroupState load(GroupId groupId);

}
