package pl.mk.gs.bl.spi.data;

import java.util.List;

import pl.mk.gs.bl.api.GroupId;
import pl.mk.gs.bl.api.UserId;

public class GroupState {

	private GroupId id;
	private String name;
	private List<UserId> usersList;
	private UserId groupOwner;

	public void addUser(UserId userId) {
		this.usersList.add(userId);
	}

	public GroupId getId() {
		return id;
	}

	public void setId(GroupId id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<UserId> getUsersList() {
		return usersList;
	}

	public void setUsersList(List<UserId> usersList) {
		this.usersList = usersList;
	}

	public UserId getGroupOwner() {
		return groupOwner;
	}

	public void setGroupOwner(UserId groupOwner) {
		this.groupOwner = groupOwner;
	}

}
