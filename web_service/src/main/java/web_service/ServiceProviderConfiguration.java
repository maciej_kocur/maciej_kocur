package web_service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import pl.mk.gs.bl.spi.GroupDao;
import pl.mk.gs.bl.spi.UserDao;

@Configuration
@EnableWebMvc
public class ServiceProviderConfiguration {
	
	@Bean
	public SettableCurrentUserService currentUserService() {
		return new SettableCurrentUserService();
	}
	
	@Bean
	public CurrentUserSettingInterceptor currentUserSettingInterceptor(SettableCurrentUserService settableCurrentUserService) {
		return new CurrentUserSettingInterceptor(settableCurrentUserService);
	}

	@Bean
	public UserDao userDao() {
		return new TempUserDao();
	}

	@Bean
	public GroupDao groupDao() {
		return new TempGroupDao();
	}
}
