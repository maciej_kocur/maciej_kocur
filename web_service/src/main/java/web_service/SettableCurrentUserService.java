package web_service;

import pl.mk.gs.bl.api.UserId;
import pl.mk.gs.bl.spi.CurrentUserService;

public class SettableCurrentUserService implements CurrentUserService {

	private UserId currentUserId;

	@Override
	public UserId getCurrentUserId() {
		return currentUserId;
	}

	public void setCurrentUserId(UserId currentUserId) {
		this.currentUserId = currentUserId;
	}

}
