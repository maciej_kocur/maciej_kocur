package web_service;

import java.util.HashMap;
import java.util.NoSuchElementException;

import pl.mk.gs.bl.api.GroupId;
import pl.mk.gs.bl.spi.GroupDao;
import pl.mk.gs.bl.spi.data.GroupState;

public class TempGroupDao implements GroupDao {
	HashMap<GroupId, GroupState> groupDatabase = new HashMap<>();

	@Override
	public void save(GroupState groupState) {
		groupDatabase.put(groupState.getId(), groupState);
		System.out.println("Dodałem do bazy: " + groupState.getName() + " o id: " + groupState.getId());
	}

	@Override
	public GroupState load(GroupId id) {
		if (groupDatabase.containsKey(id)) {
			System.out.println("Wczytuję: " + groupDatabase.get(id) + " o id: " + id);
			return groupDatabase.get(id);
		}
		throw new NoSuchElementException("There is no such group");
	}

}
