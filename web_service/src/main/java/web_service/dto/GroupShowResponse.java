package web_service.dto;

public class GroupShowResponse {
	private String groupInfo;

	public String getGroupInfo() {
		return groupInfo;
	}

	public void setGroupInfo(String groupInfo) {
		this.groupInfo = groupInfo;
	}
}
