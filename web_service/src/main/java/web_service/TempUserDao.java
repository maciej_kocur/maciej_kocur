package web_service;

import java.util.HashMap;
import java.util.NoSuchElementException;

import pl.mk.gs.bl.api.UserId;
import pl.mk.gs.bl.spi.UserDao;
import pl.mk.gs.bl.spi.data.UserState;

public class TempUserDao implements UserDao {
	HashMap<UserId, UserState> usersDatabase = new HashMap<>();

	@Override
	public void save(UserState userState) {
		usersDatabase.put(userState.getId(), userState);
	}

	@Override
	public UserState load(UserId id) {
		if (usersDatabase.containsKey(id)) {
			return usersDatabase.get(id);
		}
		throw new NoSuchElementException("There is no such user");
	}

}
