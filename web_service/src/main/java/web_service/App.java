package web_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import pl.mk.gs.bl.config.GroupServiceBusinessLogicConfiguration;

@SpringBootApplication
@Import({
		GroupServiceBusinessLogicConfiguration.class,
		ServiceProviderConfiguration.class,
		WebMvcConfiguration.class
})
public class App {

	public static void main(String args[]) {
		SpringApplication.run(App.class);
	}
}
