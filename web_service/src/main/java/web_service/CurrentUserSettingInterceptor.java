package web_service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import pl.mk.gs.bl.api.UserId;

public class CurrentUserSettingInterceptor extends HandlerInterceptorAdapter {
	private final SettableCurrentUserService settableCurrentUserService;

	@Autowired
	public CurrentUserSettingInterceptor(SettableCurrentUserService settableCurrentUserService) {
		this.settableCurrentUserService = settableCurrentUserService;
	}

	public boolean preHandle(
			HttpServletRequest request,
			HttpServletResponse response,
			Object handler) throws Exception {
		try {
			updateCurrentUser(request);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void updateCurrentUser(HttpServletRequest request) {
		settableCurrentUserService.setCurrentUserId(
				UserId.from(request.getHeader("Current-User-Id")));
	}
}