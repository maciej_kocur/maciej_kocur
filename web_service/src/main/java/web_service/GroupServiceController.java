package web_service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pl.mk.gs.bl.api.GroupId;
import pl.mk.gs.bl.api.GroupService;
import web_service.dto.GroupCreationRequest;
import web_service.dto.GroupCreationResponse;
import web_service.dto.GroupJoiningRequest;
import web_service.dto.GroupShowRequest;
import web_service.dto.GroupShowResponse;
import web_service.dto.UserRegistrationRequest;
import web_service.dto.UserRegistrationResponse;

@RestController
public class GroupServiceController {

	private final GroupService groupService;

	@Autowired
	public GroupServiceController(GroupService groupService) {
		this.groupService = groupService;
	}

	@RequestMapping(
			value = "/register",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public UserRegistrationResponse register(@RequestBody UserRegistrationRequest request) {
		UserRegistrationResponse response = new UserRegistrationResponse();
		response.setUserId(groupService.register(request.getUserName()).asString());
		return response;
	}

	@RequestMapping(
			value = "/group/new",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public GroupCreationResponse createGroup(
			@RequestBody GroupCreationRequest request) {
		GroupCreationResponse response = new GroupCreationResponse();
		response.setGroupId(groupService.createGroup(request.getGroupName()).asString());
		return response;
	}

	@RequestMapping(
			value = "/group/join",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public void joinGroup(
			@RequestBody GroupJoiningRequest request) {
		GroupId groupId = GroupId.from(request.getGroupId());
		groupService.joinGroup(groupId);
	}

	@RequestMapping(
			value = "/group/show",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public GroupShowResponse showGroup(@RequestBody GroupShowRequest request) {
		GroupShowResponse response = new GroupShowResponse();
		GroupId groupId = GroupId.from(request.getGroupId());
		response.setGroupInfo(groupService.showGroup(groupId));
		return response;
	}
}
