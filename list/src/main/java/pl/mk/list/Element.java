package pl.mk.list;

class Element<T> {
	private T item;
	private Element<T> nextElement;

	public Element(T item) {
		this.item = item;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T item) {
		this.item = item;
	}

	public Element<T> getNextElement() {
		return nextElement;
	}

	public void setNextElement(Element<T> nextElement) {
		this.nextElement = nextElement;
	}

}
