package pl.mk.list;

public class MyList<T> {
	private Element<T> first;
	private Element<T> last;
	private int size = 0;

	public MyList() {
	}

	public MyList(MyList<T> list) {
		this();
		this.addAll(list);
	}

	public boolean isEmpty() {
		return first == null;
	}

	public int size() {
		return size;
	}

	public void add(T item) {
		Element<T> nextElement = new Element<T>(item);
		if (isEmpty()) {
			first = nextElement;
			last = first;
		} else {
			last.setNextElement(nextElement);
			last = nextElement;
		}
		size++;
	}

	public void add(int index, T item) {
		checkTargetIndex(index);
		if (index == size) {
			this.add(item);
		} else {
			Element<T> newElement = new Element<T>(item);
			if (index == 0) {
				newElement.setNextElement(first);
				first = newElement;
			} else {
				Element<T> previous = this.getElement(index - 1);
				Element<T> next = previous.getNextElement();
				previous.setNextElement(newElement);
				newElement.setNextElement(next);
			}
			size++;
		}
	}

	public void addAll(MyList<T> list) {
		if (isEmpty()) {
			this.first = list.first;
			this.last = list.last;
		} else {
			this.last.setNextElement(list.first);
			this.last = list.last;
		}
		this.size += list.size();
	}

	public void addAll(int index, MyList<T> list) {
		checkTargetIndex(index);
		if (index == this.size) {
			this.addAll(list);
		} else {
			if (index == 0) {
				list.last.setNextElement(this.first);
				this.first = list.first;
			} else {
				Element<T> previous = this.getElement(index - 1);
				Element<T> next = previous.getNextElement();
				previous.setNextElement(list.first);
				list.last.setNextElement(next);
			}
			this.size += list.size();
		}
	}

	public T get(int index) {
		Element<T> element = getElement(index);
		return element.getItem();
	}

	public T remove(int index) {
		checkExistingIndex(index);
		if (index == 0) {
			Element<T> previousFirst = first;
			first = first.getNextElement();
			return previousFirst.getItem();
		} else {
			Element<T> previousElement = this.getElement(index - 1);
			Element<T> targetElement = previousElement.getNextElement();
			if (isLastIndex(index)) {
				previousElement.setNextElement(null);
				last = previousElement;
			} else {
				Element<T> nextElement = targetElement.getNextElement();
				previousElement.setNextElement(nextElement);
			}
			size--;
			return targetElement.getItem();
		}
	}

	private boolean isLastIndex(int index) {
		return index == size - 1;
	}

	private Element<T> getElement(int index) {
		checkExistingIndex(index);
		Element<T> element = first;
		for (int i = 0; i < index; i++) {
			element = element.getNextElement();
		}
		return element;
	}

	private void checkTargetIndex(int index) {
		if (!isTargetIndex(index)) {
			throw new IndexOutOfBoundsException("Imposible index!");
		}
	}

	private boolean isTargetIndex(int index) {
		return index >= 0 && index <= size;
	}

	private void checkExistingIndex(int index) {
		if (!isExistingIndex(index)) {
			throw new IndexOutOfBoundsException("There is no such index!");
		}
	}

	private boolean isExistingIndex(int index) {
		return index >= 0 && index < size;
	}
}
