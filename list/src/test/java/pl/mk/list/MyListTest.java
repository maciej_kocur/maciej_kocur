package pl.mk.list;

import org.junit.Assert;
import org.junit.Test;

public class MyListTest {
	@Test
	public void isEmpty() {
		boolean actual = createList().isEmpty();
		boolean expected = true;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void isEmptyListSizedZero() {
		int actual = createList().size();
		int expected = 0;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void createsListWithInitElements() {
		MyList<Object> previousList = createList(1, 2, 3);
		MyList<Object> actual = new MyList<Object>(previousList);
		MyList<Object> expected = createList(1, 2, 3);
		for (int i = 0; i < actual.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}
	}

	@Test
	public void addsFirstElement() {
		MyList<Object> testList = createList();
		testList.add(6);
		Object actual = testList.get(0);
		Object expected = elements(6).getItem();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void addsNextElement() {
		MyList<Object> testList = createList(1, 2);
		testList.add(3);
		Object actual = testList.get(2);
		Object expected = elements(3).getItem();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void increasesSizeByAddingElement() {
		MyList<Object> testList = createList(1, 2);
		testList.add(6);
		int actual = testList.size();
		int expected = 3;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void getsElement() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		Object actual = testList.get(2);
		Object expected = 3;
		Assert.assertEquals(expected, actual);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void getsElementOutOfBounds() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.get(6);
	}

	@Test
	public void addsElementAtZeroIndex() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.add(0, 'x');
		Object actual = testList.get(0);
		Object expected = 'x';
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void addsElementAtInnerIndex() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.add(2, 'x');
		Object actual = testList.get(2);
		Object expected = 'x';
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void addsElementAtNextIndex() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.add(5, 'x');
		Object actual = testList.get(5);
		Object expected = 'x';
		Assert.assertEquals(expected, actual);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void addsElementOutOfBounds() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.add(6, 'x');
	}

	@Test
	public void increasesSizeByAddingElementAtIndex() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.add(5, 'x');
		int actual = testList.size();
		int expected = 6;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void addsList() {
		MyList<Object> testList = createList(1, 2, 3);
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(addedList);
		MyList<Object> actual = testList;
		MyList<Object> expected = createList(1, 2, 3, 4, 5, 6);
		for (int i = 0; i < testList.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}
	}

	@Test
	public void increasesSizeByAddingList() {
		MyList<Object> testList = createList(1, 2, 3);
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(addedList);
		int actual = testList.size();
		int expected = 6;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void addsListAtZeroIndex() {
		MyList<Object> testList = createList(1, 2, 3);
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(0, addedList);
		MyList<Object> actual = testList;
		MyList<Object> expected = createList(4, 5, 6, 1, 2, 3);
		for (int i = 0; i < testList.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}
	}

	@Test
	public void addsListAtInnnerIndex() {
		MyList<Object> testList = createList(1, 2, 3);
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(1, addedList);
		MyList<Object> actual = testList;
		MyList<Object> expected = createList(1, 4, 5, 6, 2, 3);
		for (int i = 0; i < testList.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}
	}

	@Test
	public void addsListAtNextIndex() {
		MyList<Object> testList = createList(1, 2, 3);
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(3, addedList);
		MyList<Object> actual = testList;
		MyList<Object> expected = createList(1, 2, 3, 4, 5, 6);
		for (int i = 0; i < testList.size(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i));
		}
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void addsListOutOfBounds() {
		MyList<Object> testList = createList(1, 2, 3);
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(4, addedList);
	}

	@Test
	public void increasesSizeByAddingListAtIndex() {
		MyList<Object> testList = createList(1, 2, 3);
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(2, addedList);
		int actual = testList.size();
		int expected = 6;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void removeElementAtFirstIndex() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.remove(0);
		Object actual = testList.get(0);
		Object expected = 2;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void removeElementAtInnerIndex() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.remove(2);
		Object actual = testList.get(2);
		Object expected = 4;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void removeElementAtLastIndex() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.remove(4);
		Object actual = testList.get(3);
		Object expected = 4;
		Assert.assertEquals(expected, actual);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void removeElementOutOfBounds() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.remove(6);
	}

	@Test
	public void decreaseSizeByElementRemoval() {
		MyList<Object> testList = createList(1, 2, 3, 4, 5);
		testList.remove(2);
		Object actual = testList.size();
		Object expected = 4;
		Assert.assertEquals(expected, actual);
	}

	private Element<Object> elements(Object object) {
		return new Element<Object>(object);
	}

	private MyList<Object> createList(Object... objects) {
		MyList<Object> list = new MyList<Object>();
		for (Object object : objects) {
			list.add(object);
		}
		return list;
	}
}
