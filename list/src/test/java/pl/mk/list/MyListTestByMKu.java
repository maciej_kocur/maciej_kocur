package pl.mk.list;

import org.junit.Assert;
import org.junit.Test;

public class MyListTestByMKu {

	private MyList<Object> createList(Object... objects) {
		MyList<Object> list = new MyList<Object>();
		for (Object object : objects) {
			list.add(object);
		}
		return list;
	}
	
	@Test (expected = IndexOutOfBoundsException.class)
	public void removeElementWithIndexEqualToSizeOfList() {
		MyList<Object> testList = createList();
		testList.remove(testList.size());
	}
	
	@Test
	public void increasesSizeByAddingListToEmptyList() {
		MyList<Object> testList = createList();
		MyList<Object> addedList = createList(4, 5, 6);
		testList.addAll(addedList);
		int actual = testList.size();
		int expected = 3;
		Assert.assertEquals(expected, actual);
	}

	
}
