package cal;

public interface Parser {
	Expression parse(Iterable<Token> tokens);
}
