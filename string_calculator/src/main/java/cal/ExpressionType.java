package cal;

public enum ExpressionType {

	NUMBER, VARIABLE, OPERATION;
}
