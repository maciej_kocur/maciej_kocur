package cal;

import cal.impl.NumericExpressionEvaluator;

public class Evaluators {
	public static Evaluator getEvaluator() {
		return new NumericExpressionEvaluator();
	}
}
