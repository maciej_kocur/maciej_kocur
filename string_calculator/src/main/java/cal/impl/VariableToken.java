package cal.impl;

import com.google.auto.value.AutoValue;

import cal.Token;
import cal.TokenType;

@AutoValue
public abstract class VariableToken implements Token {
	public abstract String value();

	public static VariableToken create(String value) {
		return new AutoValue_VariableToken(value);
	}

	@Override
	public TokenType getType() {
		return TokenType.VARIABLE;
	}
}
