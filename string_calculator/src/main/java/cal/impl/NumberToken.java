package cal.impl;

import com.google.auto.value.AutoValue;

import cal.Token;
import cal.TokenType;

@AutoValue
public abstract class NumberToken implements Token {
	public abstract double value();

	public static NumberToken create(String stringNumber) {
		return new AutoValue_NumberToken(Double.valueOf(stringNumber));
	}

	@Override
	public TokenType getType() {
		return TokenType.NUMBER;
	}
}
