package cal.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import cal.Expression;

public class ExpressionMatchingResult {
	
	private Map<VariableExpression, Expression> placeholders = new HashMap<>();
	
	private boolean successful;

	public static ExpressionMatchingResult unsuccessful() {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isSuccessful() {
		return successful;
	}
	
	private void addPlaceholder(VariableExpression name, Expression value) {
		placeholders.put(name, value);
	}
	
	private void addAllPlaceholders(Map<VariableExpression, Expression> placeholders) {
		this.placeholders.putAll(placeholders);
	}

	// TOFU change direct access to 'placeholders' field to methods : getPlaceholder() and getPlaceholders()
	public static boolean arePlaceholdersConsistent(ExpressionMatchingResult result, ExpressionMatchingResult childResult) {
		for (Entry<VariableExpression, Expression> placeholders1 : result.placeholders.entrySet()) {
			// TODO remove the train
			if (!placeholders1.getValue().equals(childResult.placeholders.get(placeholders1.getKey()))) {
				return false;
			}
		}
		return true;
	}

	// TODO what if one is successful and the other one not??
	public static ExpressionMatchingResult mergePlaceholders(ExpressionMatchingResult result, ExpressionMatchingResult childResult) {
		result.addAllPlaceholders(childResult.placeholders);
		return result;
	}

	public static ExpressionMatchingResult successful() {
		// TODO Auto-generated method stub
		return null;
	}

	public static ExpressionMatchingResult successfulWithPlaceholder(
			VariableExpression name,
			Expression value) {
		ExpressionMatchingResult result = ExpressionMatchingResult.successful();
		result.addPlaceholder(name, value);
		return result;
	}
}
