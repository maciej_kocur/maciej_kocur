package cal.impl;

import java.util.Map;

import javax.annotation.Nullable;

import com.google.common.base.Preconditions;

import autovalue.shaded.com.google.common.common.collect.ImmutableMap;
import cal.Expression;
import cal.Operator;
import cal.Parser;
import cal.Token;
import cal.TokenType;

public class RecursiveDescentParser implements Parser {

	@Override
	public Expression parse(Iterable<Token> tokens) {
		ParsingResult result = parseAdditiveExpression(Stream.create(tokens));
		result.checkNoRest();
		return result.expression();
	}

	private static ParsingResult parseAdditiveExpression(Stream<Token> tokens) {
		return parseLeftBindingExpression(OperationType.ADDITIVE, tokens);
	}

	private static ParsingResult parseMultiplicativeExpression(Stream<Token> tokens) {
		return parseLeftBindingExpression(OperationType.MULTIPLICATIVE, tokens);
	}

	private static ParsingResult parseExponentialExpression(Stream<Token> tokens) {
		return parseRightBidningExpressionType(OperationType.EXPONENTIAL, tokens);
	}

	private static ParsingResult parseAdditiveInverseExpression(Stream<Token> tokens) {
		return parseUnaryExpression(OperationType.ADDITIVE_INVERSE, tokens);
	}

	private static ParsingResult parseAtomicExpression(Stream<Token> tokens) {
		Token token = tokens.getFirst();
		Stream<Token> rest = tokens.skipFirst();
		if (token.getType() == TokenType.OPEN_PARENTHESES) {
			return parseParenthesesContent(rest);
		}
		// TODO: handle this in left-binding
		if (token.getType() == TokenType.SUBTRACT) {
			return parseAdditiveInverseExpression(tokens);

		}
		return ParsingResult.create(resolveTokenType(token), rest);
	}

	private static Expression resolveTokenType(Token token) {
		TokenType tokenType = token.getType();
		switch (tokenType) {
			case NUMBER:
				return NumberExpression.create(((NumberToken) token).value());
			case VARIABLE:
				return VariableExpression.create(((VariableToken) token).value());
			default:
				throw new IllegalStateException("Expected Number, Variable or (, but got " + tokenType);
		}
	}

	private static ParsingResult parseUnaryExpression(OperationType operationType, Stream<Token> tokens) {

		Token token = tokens.getFirst();
		@Nullable
		Operator unaryOperator = operationType.resolveOperator(token.getType());
		if (unaryOperator == null) {
			return operationType.parseNextLevelExpression(tokens);
		}
		Stream<Token> rest = tokens.skipFirst();
		ParsingResult rightExpression = parseUnaryExpression(operationType, rest);

		return ParsingResult.create(OperationExpression.create(unaryOperator, rightExpression.expression()),
				rightExpression.rest());
	}

	private static ParsingResult parseLeftBindingExpression(OperationType leftBindingOperationType,
			Stream<Token> tokens) {
		ParsingResult firstResult = leftBindingOperationType.parseNextLevelExpression(tokens);
		if (!firstResult.hasRest()) {
			return firstResult;
		}

		Expression previousLeftExpression = firstResult.expression();
		tokens = firstResult.rest();

		while (true) {

			Token next = tokens.getFirst();

			@Nullable
			Operator operator = leftBindingOperationType.resolveOperator(next.getType());
			if (operator == null) {
				return ParsingResult.create(previousLeftExpression, tokens);
			}

			ParsingResult rightResult = leftBindingOperationType.parseNextLevelExpression(tokens.skipFirst());

			OperationExpression newLeftExpression = OperationExpression.create(operator, previousLeftExpression,
					rightResult.expression());

			if (!rightResult.hasRest()) {
				return ParsingResult.create(newLeftExpression);
			}

			previousLeftExpression = newLeftExpression;
			tokens = rightResult.rest();
		}
	}

	// TODO: this does not exist in reality
	private static ParsingResult parseRightBidningExpressionType(OperationType rightBindingOperationType,
			Stream<Token> tokens) {

		ParsingResult result = rightBindingOperationType.parseNextLevelExpression(tokens);
		if (!result.hasRest()) {
			return ParsingResult.create(result.expression());
		}
		Stream<Token> rest = result.rest();
		Token next = rest.getFirst();
		TokenType nextType = next.getType();

		@Nullable
		Operator operator = rightBindingOperationType.resolveOperator(nextType);
		if (operator == null) {
			return result;
		}
		ParsingResult rightResult = parseRightBidningExpressionType(rightBindingOperationType, rest.skipFirst());
		return ParsingResult.create(OperationExpression.create(operator, result.expression(), rightResult.expression()),
				rightResult.rest());
	}

	private static ParsingResult parseParenthesesContent(Stream<Token> tokens) {
		ParsingResult innerParsingResult = parseAdditiveExpression(tokens);
		innerParsingResult.checkHasRest();
		Stream<Token> rest = innerParsingResult.rest();
		Token token = rest.getFirst();
		Preconditions.checkArgument(token.getType() == TokenType.CLOSE_PARENTHESES, "expected ) but got %s", token);
		return ParsingResult.create(innerParsingResult.expression(), rest.skipFirst());
	}

	private enum OperationType {
		ADDITIVE(ImmutableMap.of(TokenType.ADD, Operator.ADDITION, TokenType.SUBTRACT, Operator.SUBTRACTION)) {

			@Override
			public ParsingResult parseNextLevelExpression(Stream<Token> tokens) {
				return parseMultiplicativeExpression(tokens);
			}
		},
		MULTIPLICATIVE(
				ImmutableMap.of(TokenType.MULTIPLY, Operator.MULTIPLICATION, TokenType.DIVIDE, Operator.DIVISION)) {

			@Override
			public ParsingResult parseNextLevelExpression(Stream<Token> tokens) {
				return parseAdditiveInverseExpression(tokens);
			}
		},
		EXPONENTIAL(ImmutableMap.of(TokenType.POWER, Operator.EXPONENTIATION)) {

			@Override
			public ParsingResult parseNextLevelExpression(Stream<Token> tokens) {
				return parseAtomicExpression(tokens);
			}
		},
		ADDITIVE_INVERSE(ImmutableMap.of(TokenType.SUBTRACT, Operator.ADDITIVE_INVERSION)) {
			@Override
			public ParsingResult parseNextLevelExpression(Stream<Token> tokens) {
				return parseExponentialExpression(tokens);
			}
		};

		private final Map<TokenType, Operator> operatorByTokenType;

		private OperationType(Map<TokenType, Operator> operatorByTokenType) {
			this.operatorByTokenType = operatorByTokenType;
		}

		@Nullable
		public Operator resolveOperator(TokenType type) {
			return operatorByTokenType.get(type);
		}

		public abstract ParsingResult parseNextLevelExpression(Stream<Token> tokens);
	}
}