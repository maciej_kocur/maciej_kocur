package cal.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.google.auto.value.AutoValue;

import autovalue.shaded.com.google.common.common.base.Preconditions;
import cal.Expression;
import cal.ExpressionType;
import cal.Operator;

@AutoValue
public abstract class OperationExpression implements Expression {

	public abstract Operator operator();

	public abstract List<Expression> operands();

	public static OperationExpression create(Operator operator, Expression... expression) {
		return new AutoValue_OperationExpression(operator, new ArrayList<Expression>(Arrays.asList(expression)));
	}

	public Expression operand(int index) {
		return operands().get(index);
	}

	@Override
	public ExpressionType getType() {
		return ExpressionType.OPERATION;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(operator()).append(" ").append(operands().toString()).toString();
	}

	@Override
	public ExpressionMatchingResult matchPlaceholders(Expression expression) {
		if (expression.getType() != ExpressionType.OPERATION) {
			return ExpressionMatchingResult.unsuccessful();
		}
		// safe cast - checked type above
		OperationExpression operationExpression = (OperationExpression) expression;
		if (!hasSameOperatorAs(operationExpression)) {
			return ExpressionMatchingResult.unsuccessful();
		}
		return matchPlaceholdersWithinChildren(operationExpression.operands());
	}

	private ExpressionMatchingResult matchPlaceholdersWithinChildren(Iterable<Expression> expressions) {
		Iterator<Expression> expressionIterator = expressions.iterator();
		Iterator<Expression> patternIterator = operands().iterator();
		
		ExpressionMatchingResult result = new ExpressionMatchingResult();
		while (true) {
			if (expressionIterator.hasNext()) {
				Preconditions.checkState(patternIterator.hasNext());
			} else {
				Preconditions.checkState(!patternIterator.hasNext());
				break;
			}
			Expression nextExpression = expressionIterator.next();
			Expression nextPattern = patternIterator.next();
			ExpressionMatchingResult childResult = nextPattern.matchPlaceholders(nextExpression);
			if (!childResult.isSuccessful()) {
				return ExpressionMatchingResult.unsuccessful();
			}
			if (ExpressionMatchingResult.arePlaceholdersConsistent(result, childResult)) {
				result = ExpressionMatchingResult.mergePlaceholders(result, childResult);
			} else {
				return ExpressionMatchingResult.unsuccessful();
			}
		}
		return result;
	}

	private boolean hasSameOperatorAs(OperationExpression other) {
		return operator() == other.operator();
	}
}
