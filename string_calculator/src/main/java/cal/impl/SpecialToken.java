package cal.impl;

import com.google.auto.value.AutoValue;

import cal.Token;
import cal.TokenType;

@AutoValue
public abstract class SpecialToken implements Token {
	public abstract TokenType type();

	public static SpecialToken create(TokenType type) {
		return new AutoValue_SpecialToken(type);
	}

	@Override
	public TokenType getType() {
		return type();
	}
}
