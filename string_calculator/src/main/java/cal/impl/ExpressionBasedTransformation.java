package cal.impl;

import java.util.ArrayList;
import java.util.Collection;

import cal.Expression;
import cal.ExpressionType;
import cal.Transformation;

public class ExpressionBasedTransformation implements Transformation {

	private final Expression source;
	private final Expression target;

	public ExpressionBasedTransformation(Expression source, Expression target) {
		this.source = source;
		this.target = target;
	}

	@Override
	public Collection<Expression> apply(Expression expression) {

		return seekPossibleTransformations(expression);
	}

	private Collection<Expression> seekPossibleTransformations(Expression expression) {
		Collection<Expression> possibleTransformations = new ArrayList<>();
		// TODO think about naming
		ExpressionMatchingResult result = source.matchPlaceholders(expression);
		if (result.isSuccessful()) {
			possibleTransformations.add(substituteExpressions(expression, target));
		}
		possibleTransformations.addAll(seekPossibleTransformationsWithinChildren(expression));
		return possibleTransformations;
	}

	private Collection<Expression> seekPossibleTransformationsWithinChildren(Expression expression) {
		Collection<Expression> possibleTransformations = new ArrayList<>();
		if (expression.getType() == ExpressionType.OPERATION) {
			for (Expression operand : ((OperationExpression) expression).operands()) {
				possibleTransformations.addAll(seekPossibleTransformations(operand));
			}
		}
		return possibleTransformations;
	}

	private Expression substituteExpressions(Expression expression, Expression target) {
		// TODO
		return null;
	}
}
