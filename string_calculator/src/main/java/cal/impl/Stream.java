package cal.impl;

import java.util.Collections;

import com.google.auto.value.AutoValue;
import com.google.common.collect.Iterables;

@AutoValue
public abstract class Stream<T> {
	public abstract Iterable<T> tokens();

	public static <S> Stream<S> create(Iterable<S> tokens) {
		return new AutoValue_Stream<S>(tokens);
	}

	public T getFirst() {
		return Iterables.getFirst(tokens(), null);
	}

	public boolean isEmpty() {
		return Iterables.isEmpty(tokens());
	}

	public Stream<T> skipFirst() {
		return Stream.<T> create(Iterables.skip(tokens(), 1));
	}

	public static <S> Stream<S> emptyStream() {
		return Stream.<S> create(Collections.emptyList());
	}
}
