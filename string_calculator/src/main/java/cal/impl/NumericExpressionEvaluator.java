package cal.impl;

import cal.Evaluator;
import cal.Expression;

public class NumericExpressionEvaluator implements Evaluator {

	@Override
	public double evaluate(Expression expression) {
		switch (expression.getType()) {
			case NUMBER:
				return ((NumberExpression) expression).value();
			case OPERATION:
				return apply((OperationExpression) expression);
			case VARIABLE:
				throw new UnsupportedOperationException("cannot handle variables");
			default:
				throw new AssertionError();
		}
	}

	private double apply(OperationExpression operation) {
		switch (operation.operator()) {
			case ADDITIVE_INVERSION:
				return -evaluate(operation.operand(0));
			case ADDITION:
				return evaluate(operation.operand(0)) + evaluate(operation.operand(1));
			case SUBTRACTION:
				return evaluate(operation.operand(0)) - evaluate(operation.operand(1));
			case MULTIPLICATION:
				return evaluate(operation.operand(0)) * evaluate(operation.operand(1));
			case DIVISION:
				return evaluate(operation.operand(0)) / evaluate(operation.operand(1));
			case EXPONENTIATION:
				return Math.pow(evaluate(operation.operand(0)), evaluate(operation.operand(1)));
			default:
				throw new AssertionError();
		}
	}
}
