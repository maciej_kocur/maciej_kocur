package cal.impl;

import com.google.auto.value.AutoValue;
import com.google.common.base.Preconditions;

import cal.Expression;
import cal.Token;

@AutoValue
public abstract class ParsingResult {

	public abstract Expression expression();

	public abstract Stream<Token> rest();

	public static ParsingResult create(Expression expression, Stream<Token> rest) {
		return new AutoValue_ParsingResult(expression, rest);
	}

	public static ParsingResult create(Expression expression) {
		return new AutoValue_ParsingResult(expression, Stream.emptyStream());
	}

	public void checkNoRest() {
		Preconditions.checkState(!hasRest(), "expected empty rest but found %s", rest());
	}

	public void checkHasRest() {
		Preconditions.checkState(hasRest(), "expected non-empty rest");
	}

	public boolean hasRest() {
		return !rest().isEmpty();
	}
}
