package cal.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import cal.Token;
import cal.TokenType;
import cal.Tokenizer;

public class SimpleTokenizer implements Tokenizer {

	@Override
	public List<Token> tokenize(String expression) {
		List<Token> tokens = new LinkedList<>();
		String buffor = "";
		for (char ch : expression.toCharArray()) {
			if (isSpecialSymbol(ch)) {
				addTokenFromBuffor(tokens, buffor);
				buffor = "";
				tokens.add(SpecialToken.create(resolveTokenType(ch)));
			} else if (Character.isAlphabetic(ch) || Character.isDigit(ch) || ch == '.' || ch == '_') {
				buffor += ch;
				continue;
			}
		}
		addTokenFromBuffor(tokens, buffor);
		return tokens;
	}

	private TokenType resolveTokenType(char ch) {
		switch (ch) {
			case '(':
				return TokenType.OPEN_PARENTHESES;
			case ')':
				return TokenType.CLOSE_PARENTHESES;
			case '+':
				return TokenType.ADD;
			case '-':
				return TokenType.SUBTRACT;
			case '*':
				return TokenType.MULTIPLY;
			case '/':
				return TokenType.DIVIDE;
			case '^':
				return TokenType.POWER;
		}
		throw new AssertionError("Should never reach this line");
	}

	private void addTokenFromBuffor(List<Token> tokens, String buffor) {
		if (!buffor.isEmpty()) {
			if (buffor.matches("(?:\\d*\\.)?\\d+")) {
				tokens.add(NumberToken.create(buffor));
			} else {
				tokens.add(VariableToken.create(buffor));
			}
		}
	}

	private boolean isAddition(char ch) {
		return ch == '+';
	}

	private boolean isSubtraction(char ch) {
		return Objects.equals(ch, '-');
	}

	private boolean isMultiplication(char ch) {
		return Objects.equals(ch, '*');
	}

	private boolean isDivision(char ch) {
		return Objects.equals(ch, '/');
	}

	private boolean isRightParenthesis(char ch) {
		return Objects.equals(ch, ')');
	}

	private boolean isLeftParenthesis(char ch) {
		return Objects.equals(ch, '(');
	}

	private boolean isExponentiation(char ch) {
		return Objects.equals(ch, '^');
	}

	private boolean isSpecialSymbol(char ch) {
		return isAddition(ch) || isSubtraction(ch) || isMultiplication(ch) || isDivision(ch) || isExponentiation(ch)
				|| isLeftParenthesis(ch) || isRightParenthesis(ch);
	}
}
