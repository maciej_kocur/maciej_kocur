package cal.impl;

import com.google.auto.value.AutoValue;

import cal.Expression;
import cal.ExpressionType;

@AutoValue
public abstract class NumberExpression implements Expression {
	public abstract double value();

	public static NumberExpression create(double value) {
		return new AutoValue_NumberExpression(value);
	}

	@Override
	public ExpressionType getType() {
		return ExpressionType.NUMBER;
	}

	@Override
	public ExpressionMatchingResult matchPlaceholders(Expression expression) {
		return this.equals(expression) ? 
				ExpressionMatchingResult.successful() : 
				ExpressionMatchingResult.unsuccessful();
	}
}
