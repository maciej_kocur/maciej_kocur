package cal;

import java.util.Collection;

public interface Transformation {
	Collection<Expression> apply(Expression expression);
}
