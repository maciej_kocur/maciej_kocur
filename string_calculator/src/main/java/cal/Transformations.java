package cal;

import cal.impl.ExpressionBasedTransformation;
import cal.impl.NumericEvaluationTransformation;

public class Transformations {

	public static Transformation expressionBased(String sourceString, String targetString) {
		Parser parser = Parsers.getParser();
		Tokenizer tokenizer = Tokenizers.getTokenizer();
		Expression source = parser.parse(tokenizer.tokenize(sourceString));
		Expression target = parser.parse(tokenizer.tokenize(targetString));
		return new ExpressionBasedTransformation(source, target);
	}

	public static Transformation numericEvaluation() {
		return new NumericEvaluationTransformation();
	}

}
