package cal;

public interface Token {
	TokenType getType();
}
