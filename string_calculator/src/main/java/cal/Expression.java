package cal;

import cal.impl.ExpressionMatchingResult;

public interface Expression {
	ExpressionType getType();

	ExpressionMatchingResult matchPlaceholders(Expression expression);
}
