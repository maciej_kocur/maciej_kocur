package cal;

import cal.impl.RecursiveDescentParser;

public class Parsers {
	public static Parser getParser() {
		return new RecursiveDescentParser();
	}
}
