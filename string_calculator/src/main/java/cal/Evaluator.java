package cal;

public interface Evaluator {
	
	double evaluate(Expression expression);
	
}
