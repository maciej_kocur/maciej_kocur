package cal.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import cal.Expression;
import cal.Operator;
import cal.Token;
import cal.TokenType;

public class RecursiveDescentParserTest {

	@Test
	public void parsesSimpleAdditiveExpression() {
		Expression actual = createParser().parse(tokens(7, TokenType.ADD, 3));
		Expression expected = operation(Operator.ADDITION, 7, 3);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void parsesSimpleSubtractiveExpression() {
		Expression actual = createParser().parse(tokens(7, TokenType.SUBTRACT, 3));
		Expression expected = operation(Operator.SUBTRACTION, 7, 3);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void parsesSimpleMultiplicativeExpression() {
		Expression actual = createParser().parse(tokens(7, TokenType.MULTIPLY, 3));
		Expression expected = operation(Operator.MULTIPLICATION, 7, 3);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void parsesSimpleDivisiveExpression() {
		Expression actual = createParser().parse(tokens(7, TokenType.DIVIDE, 3));
		Expression expected = operation(Operator.DIVISION, 7, 3);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void parsesSimpleExponentialExpression() {
		Expression actual = createParser().parse(tokens(7, TokenType.POWER, 3));
		Expression expected = operation(Operator.EXPONENTIATION, 7, 3);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void parsesSimpleAdditivelyInversiveExpression() {
		Expression actual = createParser().parse(tokens(TokenType.SUBTRACT, 3));
		Expression expected = operation(Operator.ADDITIVE_INVERSION, 3);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void parsesSimpleParenthesesContent() {
		Expression actual = createParser()
				.parse(tokens(TokenType.OPEN_PARENTHESES, 7, TokenType.ADD, 3, TokenType.CLOSE_PARENTHESES));
		Expression expected = operation(Operator.ADDITION, 7, 3);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void obeysLeftBindingRuleForSubtraction() {
		Expression actual = createParser().parse(tokens(7, TokenType.SUBTRACT, 5, TokenType.SUBTRACT, 4));
		Expression expected = operation(Operator.SUBTRACTION, operation(Operator.SUBTRACTION, 7, 5), 4);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void obeysLeftBindingRuleForDivision() {
		Expression actual = createParser().parse(tokens(7, TokenType.DIVIDE, 5, TokenType.DIVIDE, 4));
		Expression expected = operation(Operator.DIVISION, operation(Operator.DIVISION, 7, 5), 4);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void obeysRightBindingRuleForExponentiation() {
		Expression actual = createParser().parse(tokens(7, TokenType.POWER, 5, TokenType.POWER, 4));
		Expression expected = operation(Operator.EXPONENTIATION, 7, operation(Operator.EXPONENTIATION, 5, 4));
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void obeysMultiplicationBeforeAdditionRule() {
		Expression actual = createParser().parse(tokens(7, TokenType.ADD, 5, TokenType.MULTIPLY, 4, TokenType.ADD, 2));
		Expression expected = operation(Operator.ADDITION,
				operation(Operator.ADDITION, 7, operation(Operator.MULTIPLICATION, 5, 4)), 2);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void obeysExponentiationBeforeMultiplicationRule() {
		Expression actual = createParser()
				.parse(tokens(7, TokenType.MULTIPLY, 5, TokenType.POWER, 4, TokenType.MULTIPLY, 2));
		Expression expected = operation(Operator.MULTIPLICATION,
				operation(Operator.MULTIPLICATION, 7, operation(Operator.EXPONENTIATION, 5, 4)), 2);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void obeysExponentiationBeforeAdditiveInverseRule() {
		Expression actual = createParser().parse(tokens(TokenType.SUBTRACT, 5, TokenType.POWER, 4));
		Expression expected = operation(Operator.ADDITIVE_INVERSION, operation(Operator.EXPONENTIATION, 5, 4));
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void obeysAdditiveInverseInExponentBeforeExponentiation() {
		Expression actual = createParser().parse(tokens(5, TokenType.POWER, TokenType.SUBTRACT, 4));
		Expression expected = operation(Operator.EXPONENTIATION, 5, operation(Operator.ADDITIVE_INVERSION, 4));
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void parsesParenthesesContentBeforeAnythingElse() {
		Expression actual = createParser().parse(tokens(7, TokenType.POWER, TokenType.OPEN_PARENTHESES, 5,
				TokenType.ADD, 2, TokenType.CLOSE_PARENTHESES));
		Expression expected = operation(Operator.EXPONENTIATION, 7, operation(Operator.ADDITION, 5, 2));
		Assert.assertEquals(expected, actual);
	}

	private OperationExpression operation(Operator operator, Object... objects) {
		Expression[] operands = new Expression[objects.length];
		for (int i = 0; i < objects.length; i++)
			if (objects[i] instanceof Number) {
				operands[i] = NumberExpression.create(((Number) objects[i]).doubleValue());
			} else if (objects[i] instanceof OperationExpression) {
				operands[i] = (OperationExpression) objects[i];
			} else {
				throw new AssertionError("unexpected object: " + objects[i]);
			}
		return OperationExpression.create(operator, operands);

	}

	private List<Token> tokens(Object... objects) {
		ImmutableList.Builder<Token> builder = ImmutableList.builder();
		for (Object object : objects) {
			if (object instanceof Number) {
				Number number = (Number) object;
				builder.add(NumberToken.create(String.valueOf(number)));
			} else if (object instanceof TokenType) {
				TokenType tokenType = (TokenType) object;
				builder.add(SpecialToken.create(tokenType));
			} else {
				throw new AssertionError("unexpected object: " + object);
			}
		}
		return builder.build();
	}

	private RecursiveDescentParser createParser() {
		return new RecursiveDescentParser();
	}
}
