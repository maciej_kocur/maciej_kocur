package cal.impl;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import cal.Token;
import cal.TokenType;

public class SimpleTokenizerTest {

	@Test
	public void ignoreSpaces() {
		List<Token> actual = createTokenizer().tokenize(" ");
		List<Token> expected = tokens();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void ignoreTabs() {
		List<Token> actual = createTokenizer().tokenize("	");
		List<Token> expected = tokens();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void ignoreUnsuportedCharacters() {
		List<Token> actual = createTokenizer().tokenize("=!@#$%&{}[];:'\"?><,~`|\\");
		List<Token> expected = tokens();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeIntegerNumber() {
		List<Token> actual = createTokenizer().tokenize("123");
		List<Token> expected = tokens(123);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeDoubleNumber() {
		List<Token> actual = createTokenizer().tokenize("5.6");
		List<Token> expected = tokens(5.6);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeSingleLetterVariable() {
		List<Token> actual = createTokenizer().tokenize("a");
		List<Token> expected = tokens("a");
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeMultipyLettersVariable() {
		List<Token> actual = createTokenizer().tokenize("abc");
		List<Token> expected = tokens("abc");
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeMultipyLetterDigitsOrUnderscoreVariable() {
		List<Token> actual = createTokenizer().tokenize("a_1");
		List<Token> expected = tokens("a_1");
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeMultipyLettersOrDigitsVariable() {
		List<Token> actual = createTokenizer().tokenize("a1");
		List<Token> expected = tokens("a1");
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeAdd() {
		List<Token> actual = createTokenizer().tokenize("+");
		List<Token> expected = tokens(TokenType.ADD);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeSubtract() {
		List<Token> actual = createTokenizer().tokenize("-");
		List<Token> expected = tokens(TokenType.SUBTRACT);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeMultiply() {
		List<Token> actual = createTokenizer().tokenize("*");
		List<Token> expected = tokens(TokenType.MULTIPLY);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeDivide() {
		List<Token> actual = createTokenizer().tokenize("/");
		List<Token> expected = tokens(TokenType.DIVIDE);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizePower() {
		List<Token> actual = createTokenizer().tokenize("^");
		List<Token> expected = tokens(TokenType.POWER);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeOpenParentheses() {
		List<Token> actual = createTokenizer().tokenize("(");
		List<Token> expected = tokens(TokenType.OPEN_PARENTHESES);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeCloseParentheses() {
		List<Token> actual = createTokenizer().tokenize(")");
		List<Token> expected = tokens(TokenType.CLOSE_PARENTHESES);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void tokenizeSomethingCompletlyRandom() {
		List<Token> actual = createTokenizer().tokenize("a@+1-2%/3.	& 3*5( ) b_4>");
		List<Token> expected = tokens("a", TokenType.ADD, 1, TokenType.SUBTRACT, 2, TokenType.DIVIDE, 3.3,
				TokenType.MULTIPLY, 5, TokenType.OPEN_PARENTHESES, TokenType.CLOSE_PARENTHESES, "b_4");
		Assert.assertEquals(expected, actual);
	}

	private List<Token> tokens(Object... objects) {
		ImmutableList.Builder<Token> builder = ImmutableList.builder();
		for (Object object : objects) {
			if (object instanceof Number) {
				Number number = (Number) object;
				builder.add(NumberToken.create(String.valueOf(number)));
			} else if (object instanceof TokenType) {
				TokenType tokenType = (TokenType) object;
				builder.add(SpecialToken.create(tokenType));
			} else if (object instanceof String) {
				String variable = (String) object;
				builder.add(VariableToken.create(variable));
			} else {
				throw new AssertionError("unexpected object: " + object);
			}
		}
		return builder.build();
	}

	private SimpleTokenizer createTokenizer() {
		return new SimpleTokenizer();
	}
}
