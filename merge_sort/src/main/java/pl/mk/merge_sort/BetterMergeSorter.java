package pl.mk.merge_sort;

public class BetterMergeSorter {

	private int[] array;
	private int[] tempArray;

	public static BetterMergeSorter withArray(int[] array) {
		return new BetterMergeSorter(array);
	}

	private BetterMergeSorter(int[] array) {
		this.array = new int[array.length];
		this.tempArray = new int[array.length];
		System.arraycopy(array, 0, this.array, 0, array.length);
	}

	public int[] sort() {
		sortArrayWithinRange(0, array.length - 1);
		return array;
	}

	private void sortArrayWithinRange(int startPos, int endPos) {
		int length = endPos - startPos + 1;
		if (length > 1) {
			int middlePos = startPos + (endPos - startPos) / 2;
			sortArrayWithinRange(startPos, middlePos);
			sortArrayWithinRange(middlePos + 1, endPos);
			System.arraycopy(array, startPos, tempArray, startPos, length);
			merge(startPos, middlePos, endPos);
		}
	}

	private void merge(int startPos, int middlePos, int endPos) {
		int i = startPos;
		int j = startPos;
		int k = middlePos + 1;
		while (j <= middlePos) {
			if (k > endPos || tempArray[j] <= tempArray[k]) {
				array[i++] = tempArray[j++];
			} else {
				array[i++] = tempArray[k++];
			}
		}
	}
}
