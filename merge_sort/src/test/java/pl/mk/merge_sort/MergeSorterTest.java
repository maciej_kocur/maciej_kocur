package pl.mk.merge_sort;

import org.junit.Assert;
import org.junit.Test;

public class MergeSorterTest {

	@Test
	public void sortsArray() {
		int[] actual = sort(array(5, 8, 3, 1, 6, 2, 9, 7, 4));
		int[] expected = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
		Assert.assertArrayEquals(expected, actual);
	}

	private int[] array(int... integers) {
		int[] array = new int[integers.length];
		int i = 0;
		for (int integer : integers) {
			array[i++] = integer;
		}
		return array;
	}

	private int[] sort(int[] array) {
		return BetterMergeSorter.withArray(array).sort();
	}
}
